#!/usr/bin/env python3
import random as r
from discord.ext import commands
import wolframalpha
import sys
import configparser as config

bot = commands.Bot(command_prefix="?")
wolfram_client = wolframalpha.Client("")


@bot.event
async def on_ready():
    print("-" * 6)
    print("Bot is starting")
    print("-" * 6)




@bot.command(name="celly")
async def celly():
    phrases = ["So, I put your, uh, possessions, back"]
    n = r.randint(-1, len(phrases) - 1)
    await bot.say(phrases[n])


@bot.command(name="pizza")
async def pizza():
    await bot.say("Dave, order me some Pizza as well")


@bot.command(name="roulette")
async def roulette():
    num = r.randint(0, 7)
    if num == 2:
        await bot.say("BANG")
    else:
        await bot.say("Click")



@bot.command(name="commands")
async def comm():
    comms = ["celly", "pizza", "roulette", "fibo", "convert","nif","wolfram"]
    cmd = ""
    for command in comms:
        cmd += "?" + command + "\n"
    await bot.say(cmd)


@bot.command(name="fibo")
async def fibo(n: int):
    if n > 400000000 or n < 0:
        await bot.say("")
    else:
        result = ""
        a, b = 0, 1
        while a < n:
            result += str(a) + "\n"
            a, b = b, a + b
        await bot.say(result)


@bot.command(name="convert")
async def convert(val: int, unit: str):
    unit = unit.lower()
    if val > 2000000000:
        await bot.say("Value is too high")
    else:
        if unit == "kg>lbs" and val > 0:
            res = val * 2.2046226218
            await bot.say(str(val) + " kg are " + str(round(res, 2)) + " lbs")
        if unit == "lbs>kg" and val > 0:
            res = val / 2.2046226218
            await bot.say(str(val) + " lbs are " + str(round(res, 2)) + " kg")
        if unit == "c>f":
            res = (val * 9) / 5 + 32
            await bot.say(str(val) + " Celsius are " + str(round(res, 2)) + " Fahrenheit")
        if unit == "f>c":
            res = ((val - 32) * 5) / 9
            await bot.say(str(val) + " Fahrenheit are " + str(round(res, 2)) + " Celsius")
        if unit == "mile>km" and val > 0:
            res = round(val * 1.609347, 2)
            await bot.say(str(val) + " miles are " + str(round(res, 2)) + " kilometres")
        if unit == "km>mile" and val > 0:
            res = round(val / 1.609347, 2)
            await bot.say(str(val) + " kilometres are " + str(round(res, 2)) + " kilometres")
        if unit == "c>k":
            res = val + 273.15
            await bot.say(str(val) + " Celsius are " + str(round(res, 2)) + " Kelvin")
        if unit == "k>c" and val > 0:
            res = val - 273.15
            await bot.say(str(val) + " Kelvin are " + str(round(res, 2)) + " Celsius")
        if unit == "k>f" and val > 0:
            res = val * (9 / 5) - 459.67
            await bot.say(str(val) + " Kelvin are " + str(round(res, 2)) + " Fahrenheit")
        if unit == "f>k" and val > 0:
            res = (val + 459.67) * (5 / 9)
            await bot.say(str(val) + " Fahrenheit are " + str(round(res, 2)) + " Kelvin")
        


@bot.command(name="manual")
async def man(cmd: str):
    cmd = cmd.lower()
    if cmd == "convert":
        await bot.say("USAGE \n" +
                      "?convert [Integer] [Current Unit>Unit you want to convert to]\n" +
                      "EXAMPLE: Convert Celsius to Fahrenheit \n" +
                      "? convert 60 c>f \n" +
                      "UNITS \n" +
                      "Celsius(c),Fahrenheit(f, Kelvin(k), Miles(mile),Kilometres(km), Kilogramm(kg)," +
                      "Pounds(lbs)")
    if cmd == "fibo":
        await bot.say("USAGE \n" +
                      "?fibo [Integer n] \n" +
                      "The Fibonacci sequence will run until it reaches n")

    if cmd == "roulette":
        await bot.say("Play Russian Roulette with this command")

    elif cmd == "wolfram":
        await bot.say("Query wolfram alpha.\n"
                      "?wolfram \"QUERY\"")





@bot.command(name="user")
async def love():
    await bot.say("User is a X")


@bot.command(name="wolfram")
async def wolfram(query: str):
    res = wolfram_client.query(query)
    await bot.say(next(res.results).text)


@bot.command(name="nif")
async def nif():
    phrases = ["Nif is an angle", "No puedo, nif"]
    n = r.randint(-1,len(phrases)-1)
    await bot.say(phrases[n])

try:
    bot.run("") # Insert token for discord
except KeyboardInterrupt:
    sys.exit(0)
except Exception as e:
    print(e)
    sys.exit(1)